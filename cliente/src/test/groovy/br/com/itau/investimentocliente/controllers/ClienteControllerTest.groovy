package br.com.itau.investimentocliente.controllers
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.http.MediaType

import br.com.itau.investimentocliente.repositories.ClienteRepository
import br.com.itau.investimentocliente.services.ClienteService
import br.com.itau.investimentocliente.models.Cliente
import spock.lang.Specification
import spock.mock.DetachedMockFactory
import java.util.Optional

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc

import br.com.itau.investimentocliente.repositories.ClienteRepository
import br.com.itau.investimentocliente.services.ClienteService
import spock.lang.Specification
import org.springframework.context.annotation.Bean


@WebMvcTest
class ClienteControllerTest extends Specification{
	
	@Autowired	
	MockMvc mockMvc;
	
	@Autowired
	ClienteService clienteService
	
	@Autowired
	ClienteRepository clienteRepository
	
	@TestConfiguration
	static class MockConfig {
		def factory = new DetachedMockFactory() 	
		
		@Bean
		ClienteService clienteService () {
			return factory.Mock(ClienteService)
		}
		
		@Bean
		ClienteRepository clienteRepository () {
			return factory.Mock(ClienteRepository)
		}
	}
	
	def 'deve buscar um cliente por cpf' () {
		given: 'um cliente existe na base'
		String cpf= '123.123.123-12'
		Cliente cliente = new Cliente()
		cliente.setNome('Jose')
		cliente.setCpf(cpf)
		Optional clienteOptional = Optional.of(cliente)
		
		when: 'uma busca eh realizada'
		def resposta = mockMvc.perform(get('/123.123.123-12'))
		
		then: 'retorne um cliente'
		1 * clienteService.buscar(_) >> clienteOptional
		clienteOptional.isPresent() == true
	}
	
	def 'deve inserir um novo cliente' () 
	{
		given: 'os dados do novo cliente sao informados'
		Cliente cliente = new Cliente()
		cliente.setNome('Jose')
		cliente.setCpf('123.123.123-12')
		
		when: 'um novo cadastro eh realizado'
		def resposta = mockMvc.perform(
			post('/')
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content('{"nome": "Jose", "cpf": "123.123.123-12"}')
		)
		
		then: 'insira o cliente corretamente'
		1 * clienteService.cadastrar(_) >> cliente
		resposta.andExpect(status().isCreated())
				.andExpect(jsonPath('$.nome').value('Jose'))
	}
}
