package br.com.itau.investimentocliente.services

import br.com.itau.investimentocliente.models.Cliente
import br.com.itau.investimentocliente.repositories.ClienteRepository
import spock.lang.Specification

class ClienteServiceTest extends Specification {
	
	ClienteService clienteService;
	ClienteRepository clienteRepository;
	
	def setup() {
		clienteService = new ClienteService()
		clienteRepository = Mock()
		clienteService.clienteRepository = clienteRepository
		
	}
	
	def  'deve salvar um cliente' () {
		given: 'os dados de um cliente sao informados' 
		Cliente cliente = new Cliente() 
		cliente.setNome('Jose')
		cliente.setCpf('123.123.123-12')
		
		when: 'o cliente eh salvo'
		def clienteSalvo = clienteService.cadastrar(cliente)
		
		then: 'retorne um cliente'
		1 * clienteRepository.save(_) >> cliente
		clienteSalvo != null 
	}
	
	def 'deve buscar um cliente pelo cpf' () {
		given: 'os dados dos clientes existem na base'
		String cpf = '123.123.123-12'
		Cliente cliente = new Cliente()
		cliente.setNome('Jose')
		cliente.setCpf(cpf)
		def clienteOptional = Optional.of(cliente)
		
		when: 'eh feito uma busca informando cpf'
		def clienteEncontrado = clienteService.buscar(cpf)
		
		then: 'retorne o cliente buscado'
		1 * clienteRepository.findByCpf(_) >> clienteOptional
		clienteEncontrado.isPresent() == true
	}
}
