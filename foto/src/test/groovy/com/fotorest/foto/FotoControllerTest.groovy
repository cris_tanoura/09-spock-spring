package com.fotorest.foto

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc

import com.fotorest.foto.Foto
import com.fotorest.foto.FotoRepository
import com.fotorest.foto.FotoService

import org.springframework.http.MediaType

import spock.lang.Specification
import spock.mock.DetachedMockFactory
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

@WebMvcTest
class FotoControllerTest extends Specification {
	
	@Autowired
	MockMvc mockMvc;
	
	@Autowired
	FotoService fotoService;
	
	@Autowired
	FotoRepository fotoRepository
	
	@TestConfiguration
	static class MockConfig {
		def factory = new DetachedMockFactory()
		
		@Bean
		FotoService fotoService() 	{
			return factory.Mock(FotoService)
		}
		@Bean
		FotoRepository fotoRepository() 	{
			return factory.Mock(FotoRepository)
		}
	}
	
	@WithMockUser(username="José")
	def 'deve listar todas as fotos' () {
		given: 'fotos existem na base'
		def foto = new Foto()
		def fotos = new ArrayList()
		foto.setNome('Feijao')
		fotos << foto
		
		when: 'uma consulta eh feita'
		def result = mockMvc.perform(get("/"))
		
		then: 'retorne as fotos'
		1 * fotoService.buscarTodas() >> fotos
		result.andExpect(status().isOk())
			  .andExpect(jsonPath('[0].nome').value('Feijao'))
			  
	}
	
	@WithMockUser(username="José")
	def 'deve inserir uma foto'() {
		when: 'uma nova foto eh inserida'
		def response = mockMvc.perform(
			post('/')
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content('{"nome": "Feijao"}')
			.with(csrf()) 
			)
		then: 'retorne a foto inserida'
		1 * fotoService.inserir(_) >> {Foto foto -> return foto}
		response.andExpect(status().isOk())
				.andExpect(jsonPath('$.nome').value('Feijao'))
	}
}
